import React from "react";
import "./App.css";
import {
  FaPlusSquare,
  FaMinusSquare,
  FaStop,
  FaPlay,
  FaStepBackward,
} from "react-icons/fa";

class Break extends React.Component {
  render() {
    return (
      <div id="break">
        <h1 id="break-label">Break</h1>
        <FaPlusSquare
          id="break-increment"
          onClick={(e) => this.props.handleClick(1, "break")}
        />

        <h3 id="break-length">{this.props.break}</h3>

        <FaMinusSquare
          id="break-decrement"
          onClick={(e) => this.props.handleClick(-1, "break")}
        />
      </div>
    );
  }
}

class Session extends React.Component {
  render() {
    return (
      <div id="session">
        <h1 id="session-label">Session</h1>
        <FaPlusSquare
          id="session-increment"
          onClick={(e) => this.props.handleClick(1, "session")}
        />
        <h3 id="session-length">{this.props.session}</h3>
        <FaMinusSquare
          id="session-decrement"
          onClick={(e) => this.props.handleClick(-1, "session")}
        />
      </div>
    );
  }
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      break: 5,
      session: 25,
      minutes: 25,
      seconds: 0,
      secondsAux: 60,
      running: false,
      s: true,
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleStop = this.handleStop.bind(this);
    this.handleReset = this.handleReset.bind(this);
  }

  handleClick(e, type) {
    if (type === "break") {
      this.setState({
        break:
          this.state.break + e > 60 || this.state.break + e < 1
            ? this.state.break
            : this.state.break + e,
      });
    } else {
      this.setState({
        session:
          this.state.session + e > 60 || this.state.session + e < 1
            ? this.state.session
            : this.state.session + e,
        minutes:
          this.state.session + e > 60 || this.state.session + e < 1
            ? this.state.session
            : this.state.session + e,
      });
    }
  }

  handleStop() {
    if (this.state.running === true) {
      clearInterval(this.timerId);
      this.setState({
        running: false,
      });
    } else {
      this.setState({
        running: true,
      });
      this.timerId = setInterval(() => this.tick(), 1000);
    }
  }

  tick() {
    if (this.state.secondsAux - 1 === 59) {
      if (this.state.minutes === 0) {
        if (this.state.s === true) {
          this.audioBeep.play();
          this.setState({
            minutes: this.state.break,
            seconds: 60,
            s: false,
          });
        } else {
          this.audioBeep.play();
          this.setState({
            minutes: this.state.session,
            seconds: 60,
            s: true,
          });
        }
      } else {
        this.setState({
          minutes: this.state.minutes - 1,
          seconds: (this.state.secondsAux - 1) % 60,
          secondsAux: this.state.secondsAux - 1,
        });
      }
    } else {
      this.setState({
        seconds:
          this.state.secondsAux - 1 === 0 ? 0 : this.state.secondsAux - 1,
        secondsAux:
          this.state.secondsAux - 1 === 0 ? 60 : this.state.secondsAux - 1,
      });
    }
  }

  handleReset() {
    clearInterval(this.timerId);
    this.setState({
      break: 5,
      session: 25,
      minutes: 25,
      seconds: 0,
      secondsAux: 60,
      running: false,
      s: true,
    });
    this.audioBeep.pause();
    this.audioBeep.currentTime = 0;
  }

  render() {
    return (
      <div className="App">
        <div id="timers">
          <Break handleClick={this.handleClick} break={this.state.break} />
          <div id="time">
            <h1 id="timer-label">
              {this.state.s === true ? "Session" : "Break"}
            </h1>
            <h1 id="time-left">
              {this.state.minutes.toString().length === 1
                ? "0" + this.state.minutes.toString()
                : this.state.minutes}
              :
              {this.state.seconds.toString().length === 1
                ? "0" + this.state.seconds.toString()
                : this.state.seconds}
            </h1>
            {this.state.running ? (
              <FaStop className="start" onClick={this.handleStop} />
            ) : (
              <FaPlay className="start" onClick={this.handleStop} />
            )}
            <FaStepBackward className="start" onClick={this.handleReset} />
            <audio
              id="beep"
              preload="auto"
              src="https://goo.gl/65cBl1"
              ref={(audio) => {
                this.audioBeep = audio;
              }}
            />
          </div>
          <Session
            handleClick={this.handleClick}
            session={this.state.session}
          />
        </div>
      </div>
    );
  }
}

export default App;
